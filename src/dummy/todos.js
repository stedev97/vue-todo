export const todos = [
    {
        'id': 1,
        'title': 'Sorting',
        'completed': false,
        'editing': false,
        'tags': [
            'Essential',
            'Alphabetically',
            'By Date',
        ]
    },
    {
        'id': 2,
        'title': 'Date Added',
        'completed': false,
        'editing': false,
        'tags': [
            'Essential',
        ]
    },
    {
        'id': 3,
        'title': 'Database connection',
        'completed': false,
        'editing': false,
        'tags': [
            'Essential',
        ]
    },
    {
        'id': 4,
        'title': 'Accounts',
        'completed': false,
        'editing': false,
        'tags': [
            'Essential',
        ]
    },
    {
        'id': 5,
        'title': 'Revert changes',
        'completed': true,
        'editing': false,
        'tags': [
            'Last 30 seconds',
        ]
    },
]