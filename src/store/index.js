import { createStore } from "vuex";
export default createStore({
    modules: {},
    strict: true,
    devtools: process.env.NODE_ENV !== "production"
});
