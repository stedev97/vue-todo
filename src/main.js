import firebase from "firebase/app"
import "firebase/firestore";
import axios from "axios"
import { createApp } from "vue";
import App from "./App.vue";
import store from "./store";

const firebaseConfig = {
  apiKey: "AIzaSyDsVorFtHOFkktYEC0kokfrBojtlAW6YK8",
  authDomain: "todoapp-3f012.firebaseapp.com",
  databaseURL: "https://todoapp-3f012.firebaseio.com",
  projectId: "todoapp-3f012",
  storageBucket: "todoapp-3f012.appspot.com",
  messagingSenderId: "38292402512",
  appId: "1:38292402512:web:06ce7ef08849ff2efe3c55",
  measurementId: "G-F7SXF118RR"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);

const firestore = firebaseApp.firestore();

console.log({...firestore})

import Controlbar from "@/components/Controlbar.vue";
import DarkModeSwitch from "@/components/DarkModeSwitch.vue";
import Header from "./components/Header";
import Loader from "./components/Loader";
import TextInput from "./components/TextInput";
import TodoList from "./components/TodoList";
import TodoListItem from "./components/TodoListItem";

const app = createApp(App)
    .use(store)
    .use(axios)
    .component("Controlbar", Controlbar)
    .component("DarkModeSwitch", DarkModeSwitch)
    .component("Header", Header)
    .component("Loader", Loader)
    .component("TextInput", TextInput)
    .component("TodoList", TodoList)
    .component("TodoList", TodoList)
    .component("TodoListItem", TodoListItem)

app.mount("#app");
